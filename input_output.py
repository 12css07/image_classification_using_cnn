""".."""

from __future__ import print_function
import lasagne
import theano.tensor as T
import time
import numpy as np

from classification import create_iter_functions
from classification import train
from classification import test
from preprocess import DatasetLoadAndClean
from config import *
from models import Models

import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

handler = logging.FileHandler('epoch.log')
handler.setLevel(logging.INFO)

formatter = logging.Formatter('%(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)


def main(num_epochs=NUM_EPOCHS):
    """.."""
    print("Loading data...")
    dataset = DatasetLoadAndClean().loader_divde("shoes_dataset_for_Alexnet127.pickle")

    print("Building model and compiling functions...")

    output_layer = Models().AlexNet_model(
        input_height=dataset['input_height'],
        input_width=dataset['input_width'],
        output_dim=dataset['output_dim'])

    x = lasagne.layers.get_all_layers(output_layer)

    iter_funcs = create_iter_functions(
        dataset,
        output_layer,
        x,
        X_tensor_type=T.tensor4)

    # with np.load('epoch-1new.npz') as f:
    #     param_values = [f['arr_%d' % i] for i in range(len(f.files))]

    # lasagne.layers.set_all_param_vailues(output_layer, param_values)

    # test(iter_funcs, dataset, x)

    print("Starting training...")
    now = time.time()
    try:
        for epoch in train(iter_funcs, dataset):
            logger.info("Epoch {} of {} took {:.3f}s".format(epoch['number'], num_epochs, time.time() - now))
            now = time.time()
            logger.info("  training loss:\t\t{:.6f}".format(epoch['train_loss']))
            logger.info("  validation loss:\t\t{:.6f}".format(epoch['valid_loss']))
            logger.info("  validation accuracy:\t\t{:.2f} %%".format(epoch['valid_accuracy'] * 100))

            if epoch['number'] >= num_epochs:
                break

    except KeyboardInterrupt:
        pass

    st = 'epoch' + '-1AlexNet'
    np.savez(st + '.npz', *lasagne.layers.get_all_param_values(output_layer))

    return output_layer

if __name__ == '__main__':
    main()
